const BeanstalkdWorker = require("beanstalkd-worker");
const Queues = require("./Queues");

class BeanStalkdQueues extends Queues {
    name = 'BeanStalkdQueues';

    async init() {
        await super.init();

        this.worker = new BeanstalkdWorker(
            process.env.BEANSTALKD_HOST, // beanstalkd host
            process.env.BEANSTALKD_PORT, // beanstalkd port
        );

        if (this.mode === Queues.MODE_TYPE.READ || this.mode === Queues.MODE_TYPE.READ_AND_WRITE) {
            await this.worker.handle(this.name, (data) => this.handler(data));
        }
        await this.worker.start();
    }

    async handler(data) {
        await super.handler(data);
        return Promise.resolve();
    }

    async spawn(data) {
        await this.worker.spawn(this.name, data);
        await super.spawn(data);
    }

    async stop() {
        await super.stop();
        await this.worker.stop();
    }
}

module.exports = BeanStalkdQueues