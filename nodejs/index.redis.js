const RedisQueues = require("./RedisQueues");

(async () => {
        const queues = new RedisQueues(RedisQueues.MODE_TYPE.WRITE);
        await queues.init();

        await queues.startTest();
})();

