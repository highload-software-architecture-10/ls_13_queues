class Queues {
    name = '';
    sData = {
        countWrite: 0,
        countRead: 0,
        maxCountWrite: 0,
        maxCountRead: 0,
        time: 0,
        startTime: 0
    }
    mode = -1; // 1: write, 2: read
    msgIndex = 0;

    static MODE_TYPE = {
        WRITE: 1,
        READ: 2,
        READ_AND_WRITE: 3
    }

    constructor(mode) {
        this.mode = mode;
    }

    async init() {
        console.log(`init: ${this.name}`);
    }

    async spawn(data) {
        // console.log(`spawn: ${this.name}`);

        this.sData.countWrite++;
    }

    async stop() {
    }

    async handler(data) {
        // console.log(`handler: ${this.name}`);
        this.sData.countRead++;
    }

    async startTest() {
        this.sData.startTime = Date.now();

        setInterval(() => this.printStatistics(), 1000);

        if (this.mode === Queues.MODE_TYPE.WRITE || this.mode === Queues.MODE_TYPE.READ_AND_WRITE) {
            while (true) {
                await this.spawn({
                    type: "test_tube",
                    payload: "test_tube_payload",
                    id: `test_${this.msgIndex++}`
                });
            }
        }
    }

    async stopTest() {
        await this.stop();
    }

    printStatistics() {
        this.sData.time = Date.now() - this.sData.startTime;

        this.sData.maxCountWrite = Math.max(this.sData.maxCountWrite, this.sData.countWrite);
        this.sData.maxCountRead = Math.max(this.sData.maxCountRead, this.sData.countRead);

        console.clear();
        console.log(`Write: ${this.sData.countWrite / this.sData.time * 1000} (${this.sData.maxCountWrite}) Read: ${this.sData.countRead / this.sData.time * 1000} (${this.sData.maxCountRead})`);

        this.sData = {
            countWrite: 0,
            countRead: 0,
            time: 0,
            startTime: Date.now(),
            maxCountWrite: this.sData.maxCountWrite,
            maxCountRead: this.sData.maxCountRead
        }

    }
}

module.exports = Queues;