const Queues = require("./Queues");
const Bull = require('bull');

class RedisQueues extends Queues {
    name = 'RedisQueues';
    _queue = null;

    async init() {
        await super.init();

        this._queue = new Bull(this.name, {redis: { host: process.env.REDIS_HOST, port: process.env.REDIS_PORT }});
        await this._queue.isReady();

        if (this.mode === Queues.MODE_TYPE.READ || this.mode === Queues.MODE_TYPE.READ_AND_WRITE) {
            this._queue.process((job, done) => this.handler(job, done));
        }
    }

    async handler(job, done) {
        await super.handler(job);
        done();
        return Promise.resolve();
    }

    async spawn(data) {
        await super.spawn(data);

        await this._queue.add(data);
    }

    async stop() {
        await super.stop();
    }
}

module.exports = RedisQueues;