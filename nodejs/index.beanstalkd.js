const BeanStalkdQueues = require("./BeanStalkdQueues");

(async () => {
        const queues = new BeanStalkdQueues(BeanStalkdQueues.MODE_TYPE.READ_AND_WRITE);
        await queues.init();

        await queues.startTest();
})();

