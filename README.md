beanstalkd one process 
- Only Read = 1908 rps max
- Only Write = 5631 rps max
- Read and Write = 1473 rps max / 4439 rps max

Redis (Bull npm package) rdb
- Only Read = 6354 rps max
- Only Write = 9389 rps max
- Read and Write = 5567 rps max / 7126 rps max